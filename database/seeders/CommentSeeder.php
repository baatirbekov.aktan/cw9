<?php

namespace Database\Seeders;

use App\Models\Comment;
use App\Models\Picture;
use App\Models\User;
use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{

        public function run()
    {
        $users = User::all();
        $pictures = Picture::all();
        foreach ($pictures as $picture) {
            $user = $users->except($picture->user->id)->random();
            Comment::factory()->count(3)->for($user)->for($picture)->create();
        };

    }
}
