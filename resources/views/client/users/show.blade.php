@extends('layouts.app')

@section('content')
    @can('view', $user)
        <div>
            <h3>{{__('Edit my profile')}} <a class="btn btn-outline-secondary m-lg-3"
                                      href="{{route('users.edit', ['user' => $user])}}">{{__('Edit')}}</a></h3>
        </div>
        <div>
            <h3>{{__('Create New Picture')}} <a class="btn btn-outline-primary m-lg-3"
                                      href="{{route('pictures.create')}}">{{__('Create')}}</a></h3>
        </div>
    @endcan
    <div class="row row-cols-1 row-cols-md-4">
        @foreach($user->pictures as $picture)
            <div class="col mb-4">
                <div class="card">
                    <img style="width: 100%; height: 400px" src="{{asset('/storage/' . $picture->picture)}}"
                         class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title"><a href="{{route('pictures.show', ['picture' => $picture])}}">{{$picture->name}}</a></h5>
                        <p class="card-text">{{$picture->user->name}}</p>
                    </div>
                    @can('delete', $picture)
                        <div class="card-footer">
                            <form method="POST" action="{{route('pictures.destroy', ['picture' => $picture])}}">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-outline-danger" type="submit">{{ __('Delete') }}</button>
                            </form>
                        </div>
                    @endcan
                </div>
            </div>
        @endforeach
    </div>

@endsection

