@extends('layouts.app')

@section('content')

    <div class="row row-cols-1 row-cols-md-4">
        @foreach($pictures as $picture)
            <div class="col mb-4">
                <div class="card text-center">
                    <img style="width: 100%; height: 400px" src="{{asset('/storage/' . $picture->picture)}}"
                         class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5>{{ __('Author') }}: <a
                                href="{{ route('users.show', ['user' => $picture->user ]) }}">{{$picture->user->name}}</a>
                        </h5>    <h5 class="card-title"></h5>
                        <h4><a href="{{ route('pictures.show', ['picture' => $picture ]) }}">{{$picture->name}}</a></h4>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    <div class="row justify-content-md-center p-5">
        <div class="col-md-auto">
            {{ $pictures->appends(['sort' => 'created_at'])->links('pagination::bootstrap-4') }}
        </div>
    </div>

@endsection
