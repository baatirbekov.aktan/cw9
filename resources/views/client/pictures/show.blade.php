@extends('layouts.app')

@section('content')

    <div class="card">
        <img src="{{asset('/storage/' . $picture->picture)}}" class="card-img-top" alt="...">
        <div class="card-body">
            <p class="card-text">{{$picture->name}}</p>
            <h4>{{__('Average score')}}: {{$grade}}</h4>
        </div>
    </div>

    @can('restore', $picture)
        <div class="mt-5">
            <h4>{{ __('Add Comment') }}</h4>
            <form action="{{route('comments.store')}}" method="POST">
                @csrf
                <input name="picture_id" type="hidden" value="{{$picture->id}}">
                <div class="mb-3">
                    <label for="content" class="form-label">{{__('Content')}}</label>
                    <input name="content" type="text"
                           class="form-control @if($errors->has('content')) is-invalid @endif"
                           id="content" aria-describedby="content">
                    <div id="content" class="form-text">Please, enter your comment</div>
                    @error('content')
                    <p class="text-danger">{{$message}}</p>
                    @enderror
                </div>
                <label for="grade" class="form-label">{{__('Grade')}}</label>
                <select name="grade" class="form-select mb-3 w-25 @if($errors->has('grade')) is-invalid @endif"
                        aria-label="Default select example">
                    <option selected>Open this select menu</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
                @error('grade')
                <p class="text-danger">{{$message}}</p>
                @enderror
                <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
            </form>
        </div>
    @endcan


    @foreach($comments as $comment)
        <div class="card mt-3">

            <div class="card-header d-flex">
                @can('delete', $comment)
                    <form method="POST" action="{{route('comments.destroy', ['comment' => $comment])}}">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-outline-danger" type="submit">{{ __('Delete') }}</button>
                    </form>
                @endcan
                @can('update', $comment)
                    <a class=" mx-3 btn btn-outline-success"
                       href="{{route('comments.edit', ['comment' => $comment])}}">{{ __('Edit') }}</a>
                @endcan
            </div>
            <div class="card-body">
                <h5>{{$comment->user->name}}</h5>
                <h6>{{__('Grade')}}: {{$comment->grade}}</h6>
                <p class="card-text">{{$comment->content}}</p>
                <p>{{$comment->created_at->diffForHumans()}}</p>
            </div>
        </div>
    @endforeach

@endsection
