@extends('layouts.app')
@section('content')
    @include('admin.navbar.navbar')


    <div>
        <h4>{{__('Create new picture')}} <a class="btn btn-outline-primary" href="{{route('admin.pictures.create')}}">{{__('Create')}}</a></h4>
    </div>

    <table class="table table-dark">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">{{__('Author')}}</th>
            <th scope="col">{{__('Name')}}</th>
            <th scope="col">{{__('Picture')}}</th>
            <th scope="col">{{__('Actions')}}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($pictures as $picture)
            <tr>
                <th scope="row">{{$picture->id}}</th>
                <td>{{$picture->user->name}}</td>
                <td>{{$picture->name}}</td>
                <td><img style="width: 100px; height: 100px;" src="{{asset('/storage/' . $picture->picture)}}" alt="{{$picture->picture}}"></td>
                <td>
                    <div style="display: flex;">
                        <a class="btn btn-outline-info"
                           href="{{route('admin.pictures.edit', ['picture' => $picture])}}">{{__('Edit')}}</a>
                        <a class="btn btn-outline-warning mx-3" href="{{route('admin.pictures.show', ['picture' => $picture])}}">
                            {{__('Show')}}
                        </a>
                        <form method="post" action="{{route('admin.pictures.destroy', ['picture' => $picture])}}">
                            @method('delete')
                            @csrf
                            <button type="submit" class="btn btn-outline-danger">{{__('Delete')}}</button>
                        </form>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
