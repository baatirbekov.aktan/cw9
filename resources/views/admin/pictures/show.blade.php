@extends('layouts.app')

@section('content')
    <div class="card" style="width: 18rem;">
        <img src="{{asset('/storage/' . $picture->picture)}}" class="card-img-top" alt="{{$picture->picture}}">
        <div class="card-body">
            <h5 class="card-title">{{__('Author')}} : {{$picture->user->name}}</h5>
           <p>{{__('Name')}}: {{$picture->name}}</p>
        </div>
    </div>
@endsection
