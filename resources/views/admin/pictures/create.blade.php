@extends('layouts.app')
@section('content')
    <h3 class="mb-3">{{__('Create picture')}}</h3>
    <form action="{{route('admin.pictures.store')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="input-group mb-3">
            <span class="input-group-text" id="name">{{__('Name')}}</span>
            <input id="name" name="name" type="text" class="form-control @if($errors->has('name')) is-invalid @endif" placeholder="Username" aria-label="Username" aria-describedby="name">

        </div>
        @error('name')
        <p class="text-danger">{{$message}}</p>
        @enderror
        <div class="mb-3">
            <label for="picture" class="form-label @if($errors->has('picture')) is-invalid @endif">{{__('Picture')}}</label>
            <input name="picture" class="form-control" type="file" id="picture">
        </div>
        @error('picture')
        <p class="text-danger">{{$message}}</p>
        @enderror
        <select name="user_id" class="form-select mb-3 w-25 @if($errors->has('user_id')) is-invalid @endif"
                aria-label="Default select example">
            @foreach($users as $user)
                <option value={{$user->id}}>{{$user->name}}</option>
            @endforeach
        </select>
        @error('user_id')
        <p class="text-danger">{{$message}}</p>
        @enderror
        <div class="form-group">
            <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
        </div>
    </form>
@endsection

