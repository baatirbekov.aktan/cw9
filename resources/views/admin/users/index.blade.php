@extends('layouts.app')
@section('content')
    @include('admin.navbar.navbar')
    <table class="table table-dark">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">{{__('Name')}}</th>
            <th scope="col">{{__('Email')}}</th>
            <th scope="col">{{__('Actions')}}</th>
            <th scope="col">{{__('Admin')}}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <th scope="row">{{$user->id}}</th>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td style="display: flex;">
                    <a class="btn btn-outline-info"
                       href="{{route('admin.users.edit', ['user' => $user])}}">{{__('Edit')}}</a>
                    <a class="btn btn-outline-warning mx-3" href="{{route('admin.users.show', ['user' => $user])}}">
                        {{__('Show')}}
                    </a>
                    @can('forceDelete', $user)
                        <form method="post" action="{{route('admin.users.destroy', ['user' => $user])}}">
                            @method('delete')
                            @csrf
                            <button type="submit" class="btn btn-outline-danger">{{__('Delete')}}</button>
                        </form>
                    @endcan

                </td>
                <td>
                    @can('forceDelete', $user)
                        <form method="post" action="{{route('admin.users.admin', ['user' => $user])}}">
                            @csrf
                            @if($user->is_admin)
                                <button type="submit" class="btn btn-outline-secondary">{{__('Deactivate')}}</button>
                            @else
                                <button type="submit" class="btn btn-outline-success">{{__('Activate')}}</button>
                            @endif
                        </form>
                    @endcan
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
