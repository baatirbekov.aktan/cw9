@extends('layouts.app')

@section('content')

    <div class="card mt-3">

        <div class="card-header">
            <h5>{{$user->name}}</h5>
        </div>
        <div class="card-body">
            <h6>{{__('Email')}}: {{$user->email}}</h6>
        </div>
    </div>

@endsection
