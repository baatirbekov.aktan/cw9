<div class="my-3">
    <a class="btn btn-outline-primary" href="{{route('admin.users.index')}}">{{__('Users')}}</a>
    <a class="btn btn-outline-primary mx-1" href="{{route('admin.comments.index')}}">{{__('Comments')}}</a>
    <a class="btn btn-outline-primary" href="{{route('admin.pictures.index')}}">{{__('Pictures')}}</a>
</div>
