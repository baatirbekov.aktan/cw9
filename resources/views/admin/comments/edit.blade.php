@extends('layouts.app')

@section('content')

    <div class="mt-5">
        <h4>{{ __('Edit') }}</h4>
        <form action="{{route('admin.comments.update', ['comment' => $comment])}}" method="POST">
            @csrf
            @method('PUT')
            <div class="mb-3">
                <label for="content" class="form-label">{{__('Content')}}</label>
                <input value="{{$comment->content}}" name="content" type="text" class="form-control @if($errors->has('content')) is-invalid @endif" id="content" aria-describedby="content">
                <div id="content" class="form-text">{{__('Please, enter your comment')}}</div>
                @error('content')
                <p class="text-danger">{{$message}}</p>
                @enderror
            </div>
            <label for="grade" class="form-label">{{__('Grade')}}</label>
            <select name="grade" class="form-select mb-3 w-25 @if($errors->has('grade')) is-invalid @endif" aria-label="Default select example">
                <option @if($comment->grade == 1) selected  @endif value="1">1</option>
                <option @if($comment->grade == 2) selected  @endif value="2">2</option>
                <option @if($comment->grade == 3) selected  @endif value="3">3</option>
                <option @if($comment->grade == 4) selected  @endif value="4">4</option>
                <option @if($comment->grade == 5) selected  @endif value="5">5</option>
            </select>
            @error('grade')
            <p class="text-danger">{{$message}}</p>
            @enderror
            <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
        </form>
    </div>

@endsection
