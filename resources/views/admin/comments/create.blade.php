@extends('layouts.app')

@section('content')

    <div class="mt-5">
        <h4>{{ __('Add Comment') }}</h4>
        <form action="{{route('admin.comments.store')}}" method="POST">
            @csrf
            <div class="mb-3">
                <label for="content" class="form-label">{{__('Content')}}</label>
                <input name="content" type="text"
                       class="form-control @if($errors->has('content')) is-invalid @endif"
                       id="content" aria-describedby="content">
                <div id="content" class="form-text">Please, enter your comment</div>
                @error('content')
                <p class="text-danger">{{$message}}</p>
                @enderror
            </div>
            <label for="grade" class="form-label">{{__('Grade')}}</label>
            <select name="grade" class="form-select mb-3 w-25 @if($errors->has('grade')) is-invalid @endif"
                    aria-label="Default select example">
                <option selected>Open this select menu</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
            </select>
            @error('grade')
            <p class="text-danger">{{$message}}</p>
            @enderror
            <label for="picture_id" class="form-label">{{__('Picture')}}</label>
            <select name="picture_id" class="form-select mb-3 w-25 @if($errors->has('picture_id')) is-invalid @endif"
                    aria-label="Default select example">
                @foreach($pictures as $picture)
                    <option value={{$picture->id}}>{{$picture->name}}</option>
                @endforeach
            </select>
            @error('picture_id')
            <p class="text-danger">{{$message}}</p>
            @enderror
            <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
        </form>
    </div>

@endsection
