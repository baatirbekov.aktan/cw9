@extends('layouts.app')
@section('content')
    @include('admin.navbar.navbar')


    <div>
        <h4>{{__('Create new comment')}} <a class="btn btn-outline-primary" href="{{route('admin.comments.create')}}">{{__('Create')}}</a></h4>
    </div>

    <table class="table table-dark">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">{{__('Name')}}</th>
            <th scope="col">{{__('Grade')}}</th>
            <th scope="col">{{__('Picture')}}</th>
            <th scope="col">{{__('Actions')}}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($comments as $comment)
            <tr>
                <th scope="row">{{$comment->id}}</th>
                <td>{{$comment->user->name}}</td>
                <td>{{$comment->grade}}</td>
                <td>{{$comment->picture->name}}</td>
                <td style="display: flex;">
                    <a class="btn btn-outline-info"
                       href="{{route('admin.comments.edit', ['comment' => $comment])}}">{{__('Edit')}}</a>
                    <a class="btn btn-outline-warning mx-3" href="{{route('admin.comments.show', ['comment' => $comment])}}">
                        {{__('Show')}}
                    </a>
                        <form method="post" action="{{route('admin.comments.destroy', ['comment' => $comment])}}">
                            @method('delete')
                            @csrf
                            <button type="submit" class="btn btn-outline-danger">{{__('Delete')}}</button>
                        </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
