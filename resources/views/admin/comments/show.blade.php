@extends('layouts.app')

@section('content')

    <div class="card mt-3">

        <div class="card-header">
            <h5>{{__('Author')}}: {{$comment->user->name}}</h5>
        </div>
        <div class="card-body">
            <h6>{{__('Grade')}}: {{$comment->grade}}</h6>
            <p class="card-text">{{$comment->content}}</p>
            <p>{{$comment->created_at->diffForHumans()}}</p>
        </div>
    </div>

@endsection
