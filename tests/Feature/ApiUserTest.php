<?php

namespace Tests\Feature;

use App\Models\Picture;
use App\Models\User;
use Laravel\Passport\Passport;
use Tests\TestCase;

class ApiUserTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_can_see_all_users()
    {
        \App\Models\User::factory(15)->has(
            Picture::factory()->count(3)
        )->create();
        $user = User::factory()->state(['is_admin' => true])->create();
        Passport::actingAs($user, ['*']);
        $response = $this->getJson(route('api.users.index'));
        $response->assertStatus(200);
        $response->assertJsonStructure(
            [
                'data',
                'meta' => [
                    'current_page',
                    'total',
                    'per_page',
                    'from',
                    'last_page',
                    'links',
                    'path',
                    'to'
                ],
                'links'
            ]
        );
    }


    /**
     *
     */
    public function test_user_can_see_one_user()
    {
        \App\Models\User::factory(5)->has(
            Picture::factory()->count(3)
        )->create();
        $user = User::factory()->create();
        Passport::actingAs($user, ['*']);
        $response = $this->getJson(route('api.users.show', ['user' => $user]));
        $response->assertOk();
        $response->assertJsonStructure(
            [
                'data' => [
                    'id',
                    'name',
                    'email'
                ]
            ]
        );
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_not_auth_user_can_see_one_user()
    {
        \App\Models\User::factory(5)->has(
            Picture::factory()->count(3)
        )->create();
        $user = User::factory()->state(['is_admin' => true])->create();
        $response = $this->getJson(route('api.users.show', ['user' => $user]));
        $response->assertUnauthorized();
    }


    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_auth_user_can_not_delete_himself()
    {
        \App\Models\User::factory(5)->has(
            Picture::factory()->count(3)
        )->create();
        $user = User::factory()->state(['is_admin' => true])->create();
        Passport::actingAs($user, ['*']);
        $response = $this->deleteJson(route('api.users.destroy', ['user' => $user]));
        $response->assertForbidden();
    }
}
