<?php

namespace Tests\Feature;

use App\Models\Comment;
use App\Models\Picture;
use App\Models\User;
use Laravel\Passport\Passport;
use Tests\TestCase;

class ApiCommentsTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_can_see_all_comments()
    {
        \App\Models\User::factory(10)->has(
            Picture::factory()->count(3)
        )->create();
        $user = User::factory()->state(['is_admin' => true])->create();
        Passport::actingAs($user, ['*']);
        $comments = Comment::factory()->count(10)->for(User::all()->random())->for(Picture::all()->random())->create();
        $response = $this->getJson(route('api.comments.index'));
        $response->assertStatus(200);
        $response->assertJsonStructure(
            [
                'data',
                'meta' => [
                    'current_page',
                    'total',
                    'per_page',
                    'from',
                    'last_page',
                    'links',
                    'path',
                    'to'
                ],
                'links'
            ]
        );
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_can_not_see_more_than_three_comments()
    {
        \App\Models\User::factory(10)->has(
            Picture::factory()->count(3)
        )->create();
        $user = User::factory()->state(['is_admin' => true])->create();
        $comments = Comment::factory()->count(10)->for(User::all()->random())->for(Picture::all()->random())->create();
        Passport::actingAs($user, ['*']);
        $response = $this->getJson(route('api.comments.index'));
        $response->assertStatus(200);
        $response->assertJsonStructure(
            [
                'data',
                'meta' => [
                    'current_page',
                    'total',
                    'per_page',
                    'from',
                    'last_page',
                    'links',
                    'path',
                    'to'
                ],
                'links'
            ]
        );
        $this->assertCount(3, $response->json('data'));
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_user_success_get_one_comment()
    {
        \App\Models\User::factory(10)->has(
            Picture::factory()->count(3)
        )->create();
        $user = User::factory()->state(['is_admin' => true])->create();
        $comments = Comment::factory()->count(10)->for(User::all()->random())->for(Picture::all()->random())->create();
        Passport::actingAs($user, ['*']);
        $response = $this->getJson(route('api.comments.show', ['comment' => $comments->random()]));
        $response->assertStatus(200);
        $response->assertJsonStructure(
            [
                'data' => [
                    'id',
                    'content',

                    'user' => [
                        'id',
                        'name',
                        'email'
                    ],
                    'picture' => [
                        'id',
                        'picture',
                        'user' => [
                            'id',
                            'name',
                            'email'
                        ]
                    ]

                ]
            ]
        );
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_not_auth_user_trying_get_one_comment()
    {
        \App\Models\User::factory(10)->has(
            Picture::factory()->count(3)
        )->create();
        $comments = Comment::factory()->count(10)->for(User::all()->random())->for(Picture::all()->random())->create();
        $response = $this->getJson(route('api.comments.show', ['comment' => $comments->random()]));
        $response->assertUnauthorized();
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_user_success_delete_comment()
    {
        \App\Models\User::factory(10)->has(
            Picture::factory()->count(3)
        )->create();
        $user = User::factory()->state(['is_admin' => true])->create();
        $comments = Comment::factory()->count(10)->for($user)->for(Picture::all()->random())->create();
        Passport::actingAs($user, ['*']);
        $comment = $comments->random();
        $this->assertDatabaseHas('comments', [
            'content' => $comment->content,
            'picture_id' => $comment->picture_id,
            'user_id' => $comment->user_id,
            'id' => $comment->id
        ]);
        $response = $this->deleteJson(route('api.comments.destroy', ['comment' => $comment]));
        $response->assertNoContent();
        $this->assertDatabaseMissing('comments', [
            'content' => $comment->content,
            'user_id' => $comment->user_id,
            'id' => $comment->id
        ]);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_user_can_not_delete_not_his_comment()
    {
        \App\Models\User::factory(10)->has(
            Picture::factory()->count(3)
        )->create();
        $user = User::factory()->state(['is_admin' => true])->create();
        $another_user = User::factory()->create();
        $comments = Comment::factory()->count(10)->for($user)->for(Picture::all()->random())->create();
        Passport::actingAs($another_user, ['*']);
        $comment = $comments->random();
        $this->assertDatabaseHas('comments', [
            'content' => $comment->content,
            'picture_id' => $comment->picture_id,
            'user_id' => $comment->user_id,
            'id' => $comment->id
        ]);
        $response = $this->deleteJson(route('api.comments.destroy', ['comment' => $comment]));
        $response->assertForbidden();
        $this->assertDatabaseHas('comments', [
            'content' => $comment->content,
            'picture_id' => $comment->picture_id,
            'user_id' => $comment->user_id,
            'id' => $comment->id
        ]);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_not_auth_user_can_not_delete_his_comment()
    {
        \App\Models\User::factory(10)->has(
            Picture::factory()->count(3)
        )->create();
        $user = User::factory()->state(['is_admin' => true])->create();
        $another_user = User::factory()->create();
        $comments = Comment::factory()->count(10)->for($user)->for(Picture::all()->random())->create();
        $comment = $comments->random();
        $this->assertDatabaseHas('comments', [
            'content' => $comment->content,
            'picture_id' => $comment->picture_id,
            'user_id' => $comment->user_id,
            'id' => $comment->id
        ]);
        $response = $this->deleteJson(route('api.comments.destroy', ['comment' => $comment]));
        $response->assertUnauthorized();
        $this->assertDatabaseHas('comments', [
            'content' => $comment->content,
            'picture_id' => $comment->picture_id,
            'user_id' => $comment->user_id,
            'id' => $comment->id
        ]);
    }


    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_not_auth_user_can_not_store_comment()
    {
        \App\Models\User::factory(10)->has(
            Picture::factory()->count(3)
        )->create();
        $user = User::factory()->state(['is_admin' => true])->create();
        $another_user = User::factory()->create();
        $comments = Comment::factory()->count(10)->for($user)->for(Picture::all()->random())->create();
        $comment = $comments->random();
        $picture = Picture::all()->random();
        $response = $this->postJson(route('api.comments.store'), [
            'content' => 'hello',
            'picture_id' => $picture->id,
            'grade' => 5
        ]);
        $response->assertUnauthorized();
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_auth_user_can_store_comment()
    {
        \App\Models\User::factory(10)->has(
            Picture::factory()->count(3)
        )->create();
        $user = User::factory()->state(['is_admin' => true])->create();
        $another_user = User::factory()->create();
        $comments = Comment::factory()->count(10)->for($user)->for(Picture::all()->random())->create();
        $comment = $comments->random();
        Passport::actingAs($user, ['*']);
        $picture = Picture::all()->random();
        $response = $this->postJson(route('api.comments.store'), [
            'content' => 'hello',
            'picture_id' => $picture->id,
            'grade' => 5
        ]);
        $response->assertStatus(201);
        $this->assertDatabaseHas('comments', [
            'content' => 'hello',
            'picture_id' => $picture->id,
            'user_id' => $user->id
        ]);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_auth_user_can_update_comment()
    {
        \App\Models\User::factory(10)->has(
            Picture::factory()->count(3)
        )->create();
        $user = User::factory()->state(['is_admin' => true])->create();
        $another_user = User::factory()->create();
        $comments = Comment::factory()->count(10)->for($another_user)->for(Picture::all()->random())->create();
        $comment = $comments->random();
        $picture = Picture::all()->random();
        Passport::actingAs($another_user, ['*']);
        $response = $this->putJson(route('api.comments.update', ['comment' => $comment]), [
            'content' => 'qwerty',
            'grade' => 5
        ]);
        $response->assertStatus(200);
        $response->assertJsonStructure(
            [
                'data' => [
                    'id',
                    'content',

                    'user' => [
                        'id',
                        'name',
                        'email'
                    ],
                    'picture' => [
                        'id',
                        'picture',
                        'user' => [
                            'id',
                            'name',
                            'email'
                        ]
                    ]

                ]
            ]
        );
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_not_auth_user_can_update_comment()
    {
        \App\Models\User::factory(10)->has(
            Picture::factory()->count(3)
        )->create();
        $user = User::factory()->state(['is_admin' => true])->create();
        $another_user = User::factory()->create();
        $comments = Comment::factory()->count(10)->for($another_user)->for(Picture::all()->random())->create();
        $comment = $comments->random();
        $picture = Picture::all()->random();
        $response = $this->putJson(route('api.comments.update', ['comment' => $comment]), [
            'content' => 'qwerty',
            'grade' => 5
        ]);
        $response->assertUnauthorized();

    }


    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_auth_user_can_not_update_not_his_comment()
    {
        \App\Models\User::factory(10)->has(
            Picture::factory()->count(3)
        )->create();
        $user = User::factory()->state(['is_admin' => true])->create();
        $another_user = User::factory()->create();
        $comments = Comment::factory()->count(10)->for($another_user)->for(Picture::all()->random())->create();
        $comment = $comments->random();
        Passport::actingAs($user, ['*']);
        $response = $this->putJson(route('api.comments.update', ['comment' => $comment]), [
            'content' => 'qwerty',
            'grade' => 5
        ]);
        $response->assertForbidden();
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_auth_user_can_not_update_his_comment_without_content()
    {
        \App\Models\User::factory(10)->has(
            Picture::factory()->count(3)
        )->create();
        $user = User::factory()->state(['is_admin' => true])->create();
        $another_user = User::factory()->create();
        $comments = Comment::factory()->count(10)->for($another_user)->for(Picture::all()->random())->create();
        $comment = $comments->random();
        Passport::actingAs($another_user, ['*']);
        $response = $this->putJson(route('api.comments.update', ['comment' => $comment]), [
            'grade' => 5
        ]);
        $response->assertStatus(422);

    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_auth_user_can_not_update_his_comment_without_grade()
    {
        \App\Models\User::factory(10)->has(
            Picture::factory()->count(3)
        )->create();
        $user = User::factory()->state(['is_admin' => true])->create();
        $another_user = User::factory()->create();
        $comments = Comment::factory()->count(10)->for($another_user)->for(Picture::all()->random())->create();
        $comment = $comments->random();
        Passport::actingAs($another_user, ['*']);
        $response = $this->putJson(route('api.comments.update', ['comment' => $comment]), [
            'content' => 'hello'
        ]);
        $response->assertStatus(422);
    }
}
