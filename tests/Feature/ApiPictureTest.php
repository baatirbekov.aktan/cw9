<?php

namespace Tests\Feature;

use App\Models\Picture;
use App\Models\User;
use Laravel\Passport\Passport;
use Tests\TestCase;

class ApiPictureTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_can_see_all_pictures()
    {
        \App\Models\User::factory(10)->has(
            Picture::factory()->count(3)
        )->create();
        $user = User::factory()->state(['is_admin' => true])->create();
        Passport::actingAs($user, ['*']);
        $response = $this->getJson(route('api.pictures.index'));
        $response->assertStatus(200);
        $response->assertJsonStructure(
            [
                'data',
                'meta' => [
                    'current_page',
                    'total',
                    'per_page',
                    'from',
                    'last_page',
                    'links',
                    'path',
                    'to'
                ],
                'links'
            ]
        );
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_can_not_see_more_than_three_pictures()
    {
        \App\Models\User::factory(10)->has(
            Picture::factory()->count(3)
        )->create();
        $user = User::factory()->state(['is_admin' => true])->create();
        Passport::actingAs($user, ['*']);
        $response = $this->getJson(route('api.pictures.index'));
        $response->assertStatus(200);
        $response->assertJsonStructure(
            [
                'data',
                'meta' => [
                    'current_page',
                    'total',
                    'per_page',
                    'from',
                    'last_page',
                    'links',
                    'path',
                    'to'
                ],
                'links'
            ]
        );
        $this->assertCount(4, $response->json('data'));
    }

}
