<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Use App\Http\Controllers\Admin\UserController as AdminUserController;
Use App\Http\Controllers\Admin\CommentController as AdminCommentController;
Use App\Http\Controllers\Admin\PictureController as AdminPictureController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::middleware('language')->group(function () {

    Auth::routes();
    Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::resource('pictures', \App\Http\Controllers\PictureController::class)->except('edit', 'update');

    Route::resource('users', \App\Http\Controllers\UserController::class)->only('show', 'edit', 'update');

    Route::resource('comments', \App\Http\Controllers\CommentController::class)->except('index', 'create');






Route::prefix('admin')->name('admin.')->middleware(['auth', 'is_admin'])->group(function () {
    Route::resources([
        'users' => AdminUserController::class,
        'comments' => AdminCommentController::class,
        'pictures' => AdminPictureController::class
    ]);
});


Route::post('/admin/{user}/admin', [AdminUserController::class, 'admin'])->name('admin.users.admin');

});
Route::get('language/{local}', [\App\Http\Controllers\LanguageSwitchController::class, 'switcher'])->name('language.switcher')->where('locale', 'en|ru|es|fr|kg');
