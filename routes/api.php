<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::prefix('api')->name('api')->apiResource('comments', \App\Http\Controllers\Api\CommentsController::class);

Route::prefix('api')->name('api')->apiResource('pictures', \App\Http\Controllers\Api\PictureController::class);

Route::prefix('api')->name('api')->apiResource('users', \App\Http\Controllers\Api\UserController::class);
