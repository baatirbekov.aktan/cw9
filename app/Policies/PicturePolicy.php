<?php

namespace App\Policies;

use App\Models\Picture;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class PicturePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can delete the model.
     *
     * @param User $user
     * @param Picture $picture
     * @return Response|bool
     */
    public function delete(User $user, Picture $picture)
    {
        return $user->id == $picture->user_id;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param User $user
     * @param Picture $picture
     * @return Response|bool
     */
    public function restore(User $user, Picture $picture)
    {
        return $user->id != $picture->user_id;
    }

}
