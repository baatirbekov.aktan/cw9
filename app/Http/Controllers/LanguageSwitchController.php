<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class LanguageSwitchController extends Controller
{
    /**
     * @param Request $request
     * @param string $locale
     * @return RedirectResponse
     */
    public function switcher(Request $request, $locale)
    {
        $request->session()->put('locale', $locale);
        return back();
    }
}
