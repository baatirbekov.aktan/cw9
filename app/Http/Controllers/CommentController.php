<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentTableEditRequest;
use App\Http\Requests\CommentTableStoreRequest;
use App\Models\Comment;
use App\Models\Picture;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param CommentTableStoreRequest $request
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function store(CommentTableStoreRequest $request)
    {
        $this->authorize('restore', Picture::find($request->input('picture_id')));
        $comment = new Comment($request->all());
        $comment->user_id = Auth::id();
        $comment->save();
        return redirect()->back()->with('success', __('Successfully created'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Comment $comment
     * @return Application|Factory|View
     */
    public function edit(Comment $comment)
    {
        $this->authorize('update', $comment);
        return view('client.comments.edit', compact('comment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CommentTableEditRequest $request
     * @param Comment $comment
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function update(CommentTableEditRequest $request, Comment $comment)
    {
        $this->authorize('update', $comment);
        $comment->update($request->all());
        return redirect()->route('pictures.show', ['picture' => $comment->picture])->with('success', __('Successfully updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Comment $comment
     * @return RedirectResponse
     */
    public function destroy(Comment $comment)
    {
        $this->authorize('delete', $comment);
        $comment->delete();
        return redirect()->back()->with('success', __('Successfully deleted'));
    }
}
