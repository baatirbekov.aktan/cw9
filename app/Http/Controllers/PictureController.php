<?php

namespace App\Http\Controllers;

use App\Http\Requests\PicturesTableStoreRequest;
use App\Models\Picture;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;

class PictureController extends Controller
{

    /**
     * PictureController constructor.
     */
    public function __construct()
    {
        return $this->middleware('auth')->only('create');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $pictures = Picture::orderByDesc('created_at')->paginate(4);
        return view('client.pictures.index', compact('pictures'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        return view('client.pictures.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PicturesTableStoreRequest $request
     * @return RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(PicturesTableStoreRequest $request)
    {
        $this->authorize('create', User::class);
        $data = $request->all();
        $file = $request->file('picture');
        if (!is_null($file)) {
            $path = $file->store('pictures', 'public');
            $data['picture'] = $path;
        }
        $picture = new Picture($data);
        $picture->user_id = Auth::id();
        $picture->save();
        return redirect()->route('users.show', ['user' => Auth::user()])->with('success', __('Successfully created'));
    }

    /**
     * Display the specified resource.
     *
     * @param Picture $picture
     * @return Application|Factory|View
     */
    public function show(Picture $picture)
    {
        $grade = round($picture->comments()->avg('grade'), 2);
        $comments = $picture->comments()->latest()->get();
        return view('client.pictures.show', ['picture' => $picture], compact('grade', 'comments'));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param Picture $picture
     * @return RedirectResponse
     */
    public function destroy(Picture $picture)
    {
        $this->authorize('delete', $picture);
        $picture->delete();
        return redirect()->route('users.show', ['user' => Auth::user()])->with('success', __('Successfully deleted'));
    }
}
