<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminPicturesTableEditRequest;
use App\Http\Requests\AdminPicturesTableStoreRequest;
use App\Models\Picture;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class PictureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $pictures = Picture::all();
        return view('admin.pictures.index', compact('pictures'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        $users = User::all();
        return view('admin.pictures.create', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AdminPicturesTableStoreRequest $request
     * @return RedirectResponse
     */
    public function store(AdminPicturesTableStoreRequest $request)
    {
        $data = $request->all();
        $file = $request->file('picture');
        if (!is_null($file)) {
            $path = $file->store('pictures', 'public');
            $data['picture'] = $path;
        }
        $picture = new Picture($data);
        $picture->save();
        return redirect()->route('admin.pictures.index')->with('success', __('Successfully created'));
    }

    /**
     * Display the specified resource.
     *
     * @param Picture $picture
     * @return Application|Factory|View
     */
    public function show(Picture $picture)
    {
        return view('admin.pictures.show', ['picture' => $picture]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Picture $picture
     * @return Application|Factory|View
     */
    public function edit(Picture $picture)
    {
        $users = User::all();
        return view('admin.pictures.edit', ['picture' => $picture], compact('users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AdminPicturesTableEditRequest $request
     * @param Picture $picture
     * @return RedirectResponse
     */
    public function update(AdminPicturesTableEditRequest $request, Picture $picture)
    {
        $data = $request->all();
        if ($request->hasFile('picture')) {
            $file = $request->file('picture');
            $path = $file->store('pictures', 'public');
            $data['picture'] = $path;
        }
        $picture->update($data);
        return redirect()->route('admin.pictures.index')->with('success', __('Successfully created'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Picture $picture
     * @return RedirectResponse
     */
    public function destroy(Picture $picture)
    {
        $picture->delete();
        return redirect()->back()->with('success', __('Successfully deleted'));
    }
}
