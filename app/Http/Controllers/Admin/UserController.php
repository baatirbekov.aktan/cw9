<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UsersTableEditRequest;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class UserController extends Controller
{

    /**
     * UserController constructor.
     */
    public function __construct()
    {
        return $this->middleware('is_admin');
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $users = User::all();
        return view('admin.users.index', compact('users'));
    }

    /**
     * @param User $user
     * @return Application|Factory|View
     */
    public function show(User $user)
    {
        return view('admin.users.show', compact('user'));
    }

    /**
     * @param User $user
     * @return Application|Factory|View
     */
    public function edit(User $user)
    {
        return view('admin.users.edit', compact('user'));
    }

    /**
     * @param UsersTableEditRequest $request
     * @param User $user
     * @return RedirectResponse
     */
    public function update(UsersTableEditRequest $request, User $user)
    {
        $user->update($request->all());
        return redirect()->route('admin.users.index')->with('success', __('Successfully updated'));
    }

    /**
     * @param User $user
     * @return RedirectResponse
     */
    public function destroy(User $user)
    {
        $this->authorize('forceDelete', $user);
        $user->delete();
        return redirect()->route('admin.users.index')->with('success', __('Successfully deleted'));
    }

    public function admin(User $user)
    {
        $this->authorize('forceDelete', $user);
        $user->is_admin = !$user->is_admin;
        $user->save();
        return redirect()->back()->with('success', __('Successfully updated'));
    }
}
