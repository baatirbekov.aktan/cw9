<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\PictureResource;
use App\Models\Picture;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class PictureController extends Controller
{
    /**
     * ArticleController constructor.
     */
    public function __construct()
    {
        return $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return AnonymousResourceCollection
     */
    public function index()
    {
        $pictures = Picture::paginate(4);
        return PictureResource::collection($pictures);
    }
}
