<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CommentTableStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'content' => ['bail', 'required', 'min:3', 'max:255'],
            'picture_id' => ['bail', 'required', 'exists:pictures,id'],
            'grade' => ['bail', 'required', 'in:1,2,3,4,5']
        ];
    }
}
